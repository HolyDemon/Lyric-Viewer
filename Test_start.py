import sys
from time import sleep
sys.path.append('utils')
from PyQt5 import QtCore, QtGui, QtWidgets
from Window import Ui_MainWindow
from lyrics import get_lyrics
from Info import *

if __name__ == "__main__":
    #token = get_token()
    #song_info = song_info(token)
    #song = song_info["item"]["name"]
    #artist = song_info["item"]["album"]["artists"][0]["name"]
    #complete_name = song+" "+artist
    complete_name = "Can I Be Him James Arthur"
    
    try:
        a = open("Songs/"+complete_name+".txt","r")
    except:
        print("File not created yet\nPlease wait while we make one")
        a = open("Songs/"+complete_name+".txt","w")
        a.write(get_lyrics(complete_name))
        a.close()
        a = open("Songs/"+complete_name+".txt","r")
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    Lyrics = a.read().split("\n")
    MainWindow.show()
    for line in Lyrics:
        print(line)
        ui.textBrowser.setPlainText(line)
        sleep(0.5)

    sys.exit(app.exec_())
    a.close()

