# Instant-Karaoke

Collects the lyrics of the current spotify song and shows it in a new window. 

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. Currently no deployment options and project is being left to work on a more optimized version with react-web. See [react-lyric-viewer](https://gitlab.com/HolyDemon/react-lyric-viewer).

### Prerequisites
```
python 3.7
spotipy
PyQt5
bs4
lxml
pip
```

### Installing

Download the code using

```
    git clone https://github.com/holyfiddlex/Lyric-Viewer.git
```

Download the dependencies (can work in virtualenv)

```
    pip install spotipy pyqt5 bs4 lxml
```

## Built With

* [Spotipy](https://github.com/plamere/spotipy) - Python library which connects to the spotify api.
* [PyQt5](https://www.tutorialspoint.com/pyqt/) - Python library used to make QT GUI interfaces
* [BS4](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) - Beautiful Soup: Used to parse text.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Inspired and based on bhrigu123's [Instant-Lyrics](https://github.com/bhrigu123/Instant-Lyrics)

# Take Aways

Using the spotipy library might not be the best option yet since the project is not as complete as the original javascript version.

Could not get pyqt to work under fast api updates, slows down and is not sustainable.

Works much better with time stamped song files but could not find a reliable way of obtaining such files other than manually creating them. (Future Project Idea?)
