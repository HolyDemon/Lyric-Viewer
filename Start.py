import sys
sys.path.append('utils')
from PyQt5 import QtCore, QtGui, QtWidgets
from window import Ui_MainWindow
from lyrics import get_lyrics
from Info import *
time_lapse = 1000
if __name__ == "__main__":    
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.showFullScreen()
    timer = QtCore.QTimer()
    timer.timeout.connect(ui.rewriteText)
    timer.start(time_lapse)
    
    sys.exit(app.exec_())
        
