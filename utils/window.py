# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'Instant Karaoke2.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from lyrics import get_lyrics
from Info import *

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(824, 645)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.textEdit = QtWidgets.QTextEdit(self.centralwidget)
        self.textEdit.setObjectName("textEdit")
        font = QtGui.QFont()
        font.setPointSize(48)
        font.setBold(True)
        font.setWeight(75)
        self.textEdit.setFont(font)
        self.textEdit.setAlignment(QtCore.Qt.AlignCenter)
        self.label = QtWidgets.QLabel()
        self.label.setPixmap(QtGui.QPixmap('Spotify.png'))
        self.label.setGeometry(60,50,8000,400)
        self.gridLayout_2.addWidget(self.textEdit, 0, 0, 1, 1)
        self.gridLayout_2.addWidget(self.label, 1, 1, 2, 2)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 824, 30))
        self.menubar.setObjectName("menubar")
        self.menuPreferences = QtWidgets.QMenu(self.menubar)
        self.menuPreferences.setObjectName("menuPreferences")
        self.menuWindow = QtWidgets.QMenu(self.menubar)
        self.menuWindow.setObjectName("menuWindow")
        self.menuHelp = QtWidgets.QMenu(self.menubar)
        self.menuHelp.setObjectName("menuHelp")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.actionPreferences = QtWidgets.QAction(MainWindow)
        self.actionPreferences.setObjectName("actionPreferences")
        self.actionDocumentation = QtWidgets.QAction(MainWindow)
        self.actionDocumentation.setObjectName("actionDocumentation")
        self.actionExport = QtWidgets.QAction(MainWindow)
        self.actionExport.setObjectName("actionExport")
        self.actionAdd_Site = QtWidgets.QAction(MainWindow)
        self.actionAdd_Site.setObjectName("actionAdd_Site")
        self.actionWindow_Settings = QtWidgets.QAction(MainWindow)
        self.actionWindow_Settings.setObjectName("actionWindow_Settings")
        self.actionGet_Lyrics = QtWidgets.QAction(MainWindow)
        self.actionGet_Lyrics.setObjectName("actionGet_Lyrics")
        self.actionAdd_File = QtWidgets.QAction(MainWindow)
        self.actionAdd_File.setObjectName("actionAdd_File")
        self.actionFAQ = QtWidgets.QAction(MainWindow)
        self.actionFAQ.setObjectName("actionFAQ")
        self.actionReport_Bug = QtWidgets.QAction(MainWindow)
        self.actionReport_Bug.setObjectName("actionReport_Bug")
        self.actionAbout = QtWidgets.QAction(MainWindow)
        self.actionAbout.setObjectName("actionAbout")
        self.menuPreferences.addAction(self.actionPreferences)
        self.menuWindow.addAction(self.actionWindow_Settings)
        self.menuWindow.addAction(self.actionGet_Lyrics)
        self.menuHelp.addAction(self.actionDocumentation)
        self.menuHelp.addAction(self.actionFAQ)
        self.menuHelp.addAction(self.actionReport_Bug)
        self.menuHelp.addAction(self.actionAbout)
        self.menuFile.addAction(self.actionExport)
        self.menuFile.addAction(self.actionAdd_Site)
        self.menuFile.addAction(self.actionAdd_File)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuPreferences.menuAction())
        self.menubar.addAction(self.menuWindow.menuAction())
        self.menubar.addAction(self.menuHelp.menuAction())
        #self.setWindowIcon(QtGui.QIcon('Karaoke.png'))

        self.retranslateUi(MainWindow)
        self.spotifySetup()
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.menuPreferences.setTitle(_translate("MainWindow", "Setti&ngs"))
        self.menuWindow.setTitle(_translate("MainWindow", "Win&dow"))
        self.menuHelp.setTitle(_translate("MainWindow", "He&lp"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.actionPreferences.setText(_translate("MainWindow", "&Preferences"))
        self.actionDocumentation.setText(_translate("MainWindow", "&Documentation"))
        self.actionExport.setText(_translate("MainWindow", "&Export"))
        self.actionAdd_Site.setText(_translate("MainWindow", "&Add Site"))
        self.actionWindow_Settings.setText(_translate("MainWindow", "&Window Settings"))
        self.actionGet_Lyrics.setText(_translate("MainWindow", "&Get Lyrics"))
        self.actionAdd_File.setText(_translate("MainWindow", "Add &File"))
        self.actionFAQ.setText(_translate("MainWindow", "&FAQ"))
        self.actionReport_Bug.setText(_translate("MainWindow", "&Report Bug"))
        self.actionAbout.setText(_translate("MainWindow", "&About"))

    def spotifySetup(self):
        self.token = get_token()
        self.complete_name = ""
        self.wait = 0
        self.fulltext = False
        
    def prepare_time_stamped(self, text):
        temp = text.split("\n")
        temp = list(filter(None,temp))
        stamps = []
        try:
            for x in temp:
                stamps.append((int(x[1:7]), x[8:]))
            return stamps
        except:
            self.fulltext = True
            return temp
    
    def current_line(self, time):
        l = 0
        g = len(self.timeStamped)
        p = (l+g)//2
        while l+1 < g:
            if self.timeStamped[p][0] < time//10:
                l = p
            else:
                g = p
            p = (l+g)//2
        return self.timeStamped[l][1]
        
    def rewriteText(self):
        global time_lapse
        if self.wait != 0:
            wait-=1
            return 0        
        try:
            self.song_data = song_info(self.token)
            if not(self.song_data):
              print("no song found")
              return 0
        except:
            print("Conection Problems")
            wait = 500
            return 0
        song = self.song_data["item"]["name"]
        artist = self.song_data["item"]["album"]["artists"][0]["name"]
        complete_name = song+" "+artist
        if self.complete_name != complete_name:
            self.fulltext = False
            self.complete_name = complete_name
            try:
                f = open("Songs/"+self.complete_name+".txt","r")
            except:
                print("File not created yet\nPlease wait while we make one")
                f = open("Songs/"+self.complete_name+".txt","w")
                f.write(get_lyrics(self.complete_name))
                f.close()
                f = open("Songs/"+self.complete_name+".txt","r")
            self.text = f.read()
            f.close()
            self.timeStamped = self.prepare_time_stamped(self.text)
        _translate = QtCore.QCoreApplication.translate
        try:
            time = self.song_data["progress_ms"]
            time_lapse = 100
            cline = self.current_line(time)
            self.textEdit.setText(_translate("MainWindow", cline))
            self.textEdit.setAlignment(QtCore.Qt.AlignCenter)
        except:
            time_lapse = 3000
            if self.fulltext:
                self.textEdit.setText(_translate("MainWindow", self.text))
                self.fulltext = False

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

