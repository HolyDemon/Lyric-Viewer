import os
import sys
import json
import spotipy
import webbrowser
import spotipy.util as util
from json.decoder import JSONDecodeError

def get_token():
    # Get the username from terminal
    #username = sys.argv[1]
    username = "12174061336"
    
    scope = 'user-read-private user-read-playback-state user-modify-playback-state' 
    #User: 12174061336?si=KwK6w1_kTZC58OTs-gVwpQ

    try:
        token = util.prompt_for_user_token(username, scope) # add scope
    except (AttributeError, JSONDecodeError):
        os.remove(f".cache-{username}")
        token = util.prompt_for_user_token(username, scope) # add scope
    return token

def song_info(token):
    spotifyObject = spotipy.Spotify(auth=token)
    song_info = spotifyObject._get('me/player/currently-playing')
    return song_info
#devices = spotifyObject.devices()
#deviceID = devices['devices'][0]['id']
if __name__=="__main__":
    token = get_token()
    song = song_info(token)
    print(song["progress_ms"])
